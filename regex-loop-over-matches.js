let input = 'A string with 6 numbers: 123, 2037, 21, 26, and 0...';

let number = /\b\d+\b/g;

let match;

// lastIndex under the hood
while ((match = number.exec(input))) {
  console.log('Found ', match[0], ' at ', match.index);
}
