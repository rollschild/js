const obj = {
  count: 10,
  doSomethingLater() {
    // The traditional function binds "this" to the "obj" context.
    // so this === obj here
    setTimeout(() => {
      // Since the arrow function doesn't have its own binding and
      // setTimeout (as a function call) doesn't create a binding
      // itself, the "obj" context of the traditional function will
      // be used within.
      // more like closure I would assume
      this.count++;
      console.log(this.count);
    }, 300);
  },
};

obj.doSomethingLater();
