function strict() {
  // 'use strict';
  // Will NOT complain if the line above is omitted
  for (counter = 0; counter < 9; ++counter) {
    console.log(counter);
  }
}

strict();
