function fibonacci(num) {
  let cache = new Array(num + 1);
  cache[0] = 0;
  cache[1] = 1;
  let recursion = function(num, cache) {
    if (num === 0 || num === 1) return num;

    if (cache[num] === undefined)
      return recursion(num - 1, cache) + recursion(num - 2, cache);

    return cache[num];
  };

  return recursion(num, cache);
}

console.log(fibonacci(5));
console.log(fibonacci(4));
console.log(fibonacci(6));
console.log(fibonacci(12));
