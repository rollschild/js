let protoBunny = {
  speak(line) {
    console.log(`The ${this.type} bunny says ${line}!`);
  },
};

let cuteBunny = Object.create(protoBunny);
cuteBunny.type = 'cute';
cuteBunny.speak(`I'm cute`);

function Bunny(type) {
  this.type = type;
}

let weirdBunny = new Bunny('weird');

console.log(Object.getPrototypeOf(Bunny) === Function.prototype);
console.log(Object.getPrototypeOf(weirdBunny) === Bunny.prototype);
