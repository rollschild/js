function match(str, subStr) {
  let i = 0;
  let j = 0;
  let index = i;

  while (i < str.length) {
    if (subStr[j] === str[i]) {
      ++j;
      ++i;
    } else {
      ++i;
      index = i - j;
      j = 0;
    }

    if (j === 0) {
      i = index;
    } else {
      if (j === subStr.length) return index;
    }
  }

  return -1;
}

console.log(match('acbcdddcass', 'casss'));
