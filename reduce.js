function reduce(array, combine, start) {
  let current = start;
  array.forEach(function pro(ele) {
    current = combine(ele, current);
  });
  return current;
}

console.log(reduce([1, 2, 3, 4, 5], (a, b) => a + b, 0));
