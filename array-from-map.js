// an iterator can be converted to an arry
// ...with the Array.from() function
const map = new Map([[1, 2], [2, 4], ['what', 'is your name?']]);
console.log(Array.from(map));

const mapper = new Map([[23, 'Michael Jordan'], [24, 'Koby Bryant']]);
console.log(Array.from(mapper.values()));
console.log(Array.from(mapper.keys()));
