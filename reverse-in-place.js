function reverseVersionOne(str) {
  return str
    .trim()
    .split(' ')
    .reverse()
    .join(' ')
    .split('')
    .reverse()
    .join('');
}

function reverseVersionTwo(str) {
  return str
    .trim()
    .split('')
    .reverse()
    .join('')
    .split(' ')
    .reverse()
    .join(' ');
}

console.log(reverseVersionOne('I am a good guy!'));
console.log(reverseVersionTwo('I am a good guy!'));
