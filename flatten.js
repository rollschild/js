function flatten(value) {
  if (value === undefined || value === null || typeof value !== "object") {
    return value;
  }
  if (Array.isArray(value)) {
    return flattenArray(value);
  }

  return flattenObject(value);
}

function flattenArray(value) {
  return value.reduce((list, v) => list.concat(flatten(v)), []);
}

function flattenObject(value) {
  const flattened = {};

  Object.entries(value).forEach(([key, val]) => {
    const isObject =
      val !== null && !Array.isArray(val) && typeof val === "object";
    const flattenedVal = flatten(val);
    if (isObject) {
      Object.assign(flattened, flattenedVal);
    } else {
      flattened[key] = flattenedVal;
    }
  });

  return flattened;
}

const nested = [
  {
    a: 123,
    b: {
      c: [1, 2, 3, [4]],
    },
    d: 567,
  },
  "hello",
];
console.log(flatten(nested));
