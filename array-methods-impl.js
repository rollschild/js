Array.prototype.myMap = function (callback) {
  // access the array using `this` since `myMap` is on Array's prototype
  console.log(this);
  const array = this;
  const len = array.length;
  const result = [];
  for (let i = 0; i < len; i++) {
    const el = array[i];
    result.push(callback(el, i, array));
  }

  return result;
};

Array.prototype.myFilter = function (callback) {
  const array = this;
  const len = array.length;
  const result = [];
  for (let i = 0; i < len; i++) {
    const el = array[i];
    if (callback(el, i, array) === true) {
      result.push(el);
    }
  }
  return result;
};

Array.prototype.myReduce = function (callback, initialValue) {
  const array = this;
  if (!array) return undefined;
  const len = array.length;
  let start = 0;
  let storedValue = initialValue;
  if (initialValue === undefined) {
    start = 1;
    storedValue = array[0];
  }
  while (start < len) {
    storedValue = callback(storedValue, array[start], start, array);
    start++;
  }

  return storedValue;
};
