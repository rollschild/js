function primeFactors(num) {
  let arr = [];
  let divisor = 2;

  while (num > 2) {
    if (num % divisor === 0) {
      if (arr.indexOf(divisor) === -1) arr.push(divisor);
      // num /= divisor;
      num = num / divisor;
    } else {
      ++divisor;
    }
  }

  return arr;
}

console.log(primeFactors(69));
console.log(primeFactors(49));
console.log(primeFactors(75));
