class EventTarget {
  constructor() {
    this.subs = new Map();
  }

  addEventListener(name, callback) {
    if (!this.subs.has(name)) {
      this.subs.set(name, new Set());
    }
    this.subs.get(name)?.add(callback);
  }

  removeEventListener(name, callback) {
    this.subs.get(name)?.delete(callback);
  }

  dispatchEvent(name) {
    this.subs.get(name)?.forEach((cb) => cb());
  }
}

// Do not edit the line below.
exports.EventTarget = EventTarget;
