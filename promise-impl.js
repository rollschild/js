// An implementation of Promise itself
const PromiseImpl = (executionHandler) => {
  let handleError = () => {};
  let promiseChain = [];

  const instance = {
    then,
    catch: catcher,
  };

  function then(handleSuccess) {
    promiseChain.push(handleSuccess);
    return instance;
  }
  function catcher(errorHandler) {
    handleError = errorHandler;
  }
  const onResolve = (value) => {
    let initialValue = value;
    try {
      promiseChain.reduce((prev, curr) => {
        return curr(prev);
      }, initialValue);
    } catch (error) {
      promiseChain = [];
      onReject(error);
    }
  };
  const onReject = (error) => {
    handleError(error);
  };

  executionHandler(onResolve, onReject);

  // return {
  //   then,
  //   catch: catcher,
  // };
  return instance;
};

const fakeApi = () => {
  const user = {
    username: "guangchu",
    sport: "basketball",
    age: "88",
  };

  if (Math.random() > 0.5) {
    return {
      data: user,
      statusCode: 200,
    };
  } else {
    return {
      statusCode: 400,
      error: "Error!",
      message: "Usere does not exist!",
    };
  }
};

const makeApiCall = (url) => {
  return PromiseImpl((resolve, reject) => {
    setTimeout(() => {
      const response = fakeApi();
      console.log("response:", response);
      if (response.statusCode !== 200) {
        reject(response);
      } else {
        resolve(response.data);
      }
    }, 5000);
  });
};

makeApiCall()
  .then((result) => {
    console.log("First then:", result);
    return result;
  })
  .then((result) => {
    console.log("User's name:", result.username);
    return result;
  })
  .then((result) => {
    console.log("User's age:", result.age);
    return result.sport;
  })
  .then((result) => {
    console.log("User's sport:", result);
  })
  .then(() => console.log("Last then"))
  .catch((error) => console.error("Error happened:", error.message));
