function throttle(callback, delay) {
  let timerId;
  let lastTimeCalled = 0; // last time callback is called
  function throttled(...args) {
    const currentTime = Date.now();
    const boundCallback = callback.bind(this, ...args);
    const timePassed = currentTime - lastTimeCalled;
    if (lastTimeCalled === 0 || delay - timePassed <= 0) {
      lastTimeCalled = currentTime;
      boundCallback();
    } else {
      clearTimeout(timerId);
      timerId = setTimeout(() => {
        lastTimeCalled = Date.now();
        boundCallback();
      }, delay - timePassed);
    }
  }

  throttled.cancel = () => {
    clearTimeout(timerId);
  };

  return throttled;
}

// Do not edit the line below.
exports.throttle = throttle;
