new Promise((_, reject) => {
  reject(new Error('You failed!'));
})
  .then(val => console.log('Success handler...'))
  .catch(reason => {
    console.log('Caught failure: ' + reason);
    return 'Returned from catch()';
  })
  .then(val => {
    console.log('Success handler after catch()...' + val);
  });
