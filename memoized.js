function memoize(callback, resolver) {
  const cache = new Map();
  function generateCacheKey(...args) {
    return !resolver || typeof resolver !== "function"
      ? JSON.stringify(args)
      : resolver(...args);
  }
  function med(...args) {
    const key = generateCacheKey(...args);

    const result = cache.has(key) ? cache.get(key) : callback(...args);
    cache.set(key, result);
    return result;
  }

  med.clear = function () {
    cache.clear();
  };
  med.delete = function (...args) {
    cache.delete(generateCacheKey(...args));
  };
  med.has = function (...args) {
    return cache.has(generateCacheKey(...args));
  };

  return med;
}

// Do not edit the line below.
exports.memoize = memoize;
