class Matrix {
  constructor(height, width, element = (y, x) => undefined) {
    this.width = width;
    this.height = height;
    this.content = [];

    for (let row = 0; row < this.height; ++row) {
      for (let col = 0; col < this.width; ++col) {
        this.content[row * this.width + col] = element(row, col);
      }
    }
  }

  get(y, x) {
    return this.content[y * this.width + x];
  }

  set(y, x, value) {
    this.content[y * this.width + x] = value;
  }
}

class MatrixIterator {
  constructor(matrix) {
    this.matrix = matrix;
    this.row = 0;
    this.col = 0;
  }

  next() {
    if (this.row === this.matrix.height) {
      return {done: true};
    }

    let value = {
      y: this.row,
      x: this.col,
      value: this.matrix.get(this.row, this.col),
    };

    if (++this.col === this.matrix.width) {
      this.col = 0;
      ++this.row;
    }

    return {value, done: false};
  }
}

Matrix.prototype[Symbol.iterator] = function() {
  return new MatrixIterator(this);
};

let matrix = new Matrix(3, 4, (y, x) => `row: ${y}, col: ${x}`);

for (let {y, x, value} of matrix) {
  console.log(y, x, value);
}
