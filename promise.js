// Implementation of Promise and its various methods

// Promise.all
// returns a single Promise that resolves to an array of the results of the input promises new Promise
Promise.myAll = (promises) => {
  return new Promise((resolve, reject) => {
    if (!promises || promises.length === 0) {
      return resolve([]);
    }
    const len = promises.length;
    let count = 0;
    const results = new Array(len);

    promises.forEach((promise, index) => {
      // some arguments might not be a Promise so needs to be converted to Promise manually
      Promise.resolve(promise)
        .then((res) => {
          count += 1;
          results[index] = res;

          if (count === len) {
            resolve(results);
          }
        })
        // .catch((err) => {
        //   reject(err);
        // });
        .catch(reject);
    });
  });
};

function testPromiseAll() {
  const p1 = Promise.resolve("p1");
  const p2 = new Promise((res) => setTimeout(res, 1000, "p2"));
  const p3 = new Promise((res) => setTimeout(() => res("p3"), 3000));
  const p4 = Promise.reject("p4 rejected");
  const p5 = Promise.reject("p5 rejected");

  const pTest1 = Promise.myAll([p1, p2, p3])
    .then((res) => console.log("resloved:", res))
    .catch((err) => console.log("rejected:", err));

  const pTest2 = Promise.myAll([p1, p5, p4])
    .then((res) => console.log("resloved:", res))
    .catch((err) => console.log("rejected:", err));
}

// Promise.resolve
// ALWAYS return a Promise
Promise.myResolve = (value) => {
  if (value && typeof value === "object" && value instanceof Promise) {
    return value;
  }

  return new Promise((resolve) => {
    resolve(value);
  });
};

function testPromiseResolve() {
  console.log("Promise.myResolve");
  const p1 = Promise.myResolve("p1");
  p1.then(console.log);

  const p2 = new Promise((resolve) => resolve("p2"));
  Promise.myResolve(p2).then(console.log);

  const p3 = new Promise((_, reject) => reject("p3 rejected"));
  Promise.myResolve(p3).catch(console.error);

  // thenable object
  const p4 = {
    then: (onFulfilled, onRejected) => {
      setTimeout(() => onFulfilled("p4 resolved"), 1000);
    },
  };
  Promise.myResolve(p4).then(console.log);
}
// testPromiseResolve();

// Promise.reject
Promise.myReject = (value) => {
  return new Promise((_, reject) => reject(value));
};

function testPromiseReject() {
  Promise.myReject(new Error("FAILED!")).then(
    () => console.log("RESOLVED!"),
    (reason) => console.error("REJECTED!", reason)
  );
}
// testPromiseReject();

// Promise.race
Promise.myRace = (promises) => {
  return new Promise((res, rej) => {
    promises.forEach((promise) => {
      // wrap p with Promise
      // as soon as one promise resolves, return
      Promise.resolve(promise).then(res).catch(rej);
    });
  });
};

function testPromiseRace() {
  const p1 = new Promise((res, rej) => setTimeout(() => res("p1"), 1000));
  const p2 = new Promise((res, rej) => setTimeout(() => res("p2"), 500));
  const p = Promise.myRace([p1, p2]);
  Promise.myRace([p1, p2, 3]).then(console.log);
  // p.then(console.log);
}
// testPromiseRace();

// Promise.allSettled
// whether the promise is fully fulfilled or partially failed, it will enter the `.then()` callback of Promise.allSettled()
Promise.myAllSettled = (promises) => {
  return new Promise((resolve, reject) => {
    if (!promises || promises.length === 0) {
      return resolve([]);
    }

    const len = promises.length;
    let count = 0;
    const results = [];

    promises.forEach((promise, index) => {
      Promise.resolve(promise)
        .then((res) => {
          count += 1;

          results[index] = {
            status: "fulfilled",
            value: res,
          };

          if (count === len) {
            resolve(results);
          }
        })
        .catch((err) => {
          count += 1;
          results[index] = {
            status: "rejected",
            reason: err,
          };

          if (count === len) {
            resolve(results);
          }
        });
    });
  });
};

function testPromiseAllSettled() {
  const p1 = Promise.resolve("p1");
  const p2 = new Promise((resolve, reject) => setTimeout(resolve, 1000, 2));
  const p3 = new Promise((_, reject) => setTimeout(reject, 500, "p3"));

  const p = Promise.myAllSettled([p1, p2, p3]).then((res) =>
    console.log(JSON.stringify(res, null, 4))
  );
}

testPromiseAllSettled();
