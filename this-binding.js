Function.prototype.myCall = function (thisContext, ...args) {
  const key = Symbol();
  thisContext[key] = this;
  const res = thisContext[key](...args);
  delete thisContext[key];
  return res;
};

Function.prototype.myApply = function (thisContext, args = []) {
  const key = Symbol();
  thisContext[key] = this;
  const res = thisContext[key](...args);
  delete thisContext[key];
  return res;
};

Function.prototype.myBind = function (thisContext, ...args) {
  const fn = this;
  return function (...nextArgs) {
    return fn.myApply(thisContext, [...args, ...nextArgs]);
  };
};
