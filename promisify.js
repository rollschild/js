function promisify(callback) {
  return function (...args) {
    return new Promise((resolve, reject) => {
      const handleErrorAndValue = (error, value) => {
        if (error) {
          reject(error);
        } else {
          resolve(value);
        }
      };
      return callback.call(this, ...args, handleErrorAndValue);
    });
  };
}

// Do not edit the line below.
exports.promisify = promisify;
