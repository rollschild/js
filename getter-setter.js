class Temperature {
  constructor(celsius) {
    this.celsius = celsius;
  }

  get fahrenheit() {
    return this.celsius * 1.8 + 32;
  }

  set fahrenheit(val) {
    this.celsius = (val - 32) / 1.8;
  }

  static fromFahrenheit(temp) {
    return new Temperature((temp - 32) / 1.8);
  }
}

let temp = new Temperature(22);
console.log(temp.fahrenheit);
temp.fahrenheit = 86;
console.log(temp.celsius);

let anotherTemp = Temperature.fromFahrenheit(86);
console.log(anotherTemp.celsius);
