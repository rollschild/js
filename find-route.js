function findRoute(from, to, connections) {
  let work = [{at: from, via: null}];
  for (let i = 0; i < work.length; i++) {
    let {at, via} = work[i];

    // connections is a Map
    for (let stop of connections.get(at) || []) {
      if (stop === to) return via;
      if (!work.some(w => w.at === stop)) {
        work.push({at: stop, via: via || stop});
      }
    }
  }

  return null;
}
