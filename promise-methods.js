Promise.myRace = function (promises) {
  return new Promise((resolve, reject) => {
    promises.forEach((promise) => {
      Promise.resolve(promise).then(resolve).catch(reject);
    });
  });
};

Promise.myAny = function (promises) {
  return new Promise((resolve, reject) => {
    let rejections = 0;
    promises.forEach((promise) => {
      Promise.resolve(promise)
        .then(resolve)
        .catch((err) => {
          rejections++;
          if (rejections === promises.length) {
            reject("all promises rejected");
          }
        });
    });
  });
};

Promise.myAll = function (promises) {
  return new Promise((resolve, reject) => {
    if (!promises || promises.length === 0) {
      resolve([]);
    }
    const len = promises.length;
    let count = 0;
    const results = new Array(len);

    promises.forEach((promise, index) => {
      Promise.resolve(promise)
        .then((res) => {
          results[index] = res;
          count++;
          if (count === len) {
            resolve(results);
          }
        })
        .catch(reject);
    });
  });
};

/*
Promise.myAllSettled = function (promises) {
  return new Promise((resolve, reject) => {
    if (!promises || promises.length === 0) {
      resolve([]);
    }
    const len = promises.length;
    let count = 0;
    const results = new Array(len);

    promises.forEach((promise, index) => {
      Promise.resolve(promise)
        .then((res) => {
          results[index] = {
            status: "fulfilled",
            value: res,
          };
          count++;
          if (count === len) {
            resolve(results);
          }
        })
        .catch((err) => {
          results[index] = {
            status: "rejected",
            error: err,
          };
          count++;
          if (count === len) {
            resolve(results);
          }
        });
    });
  });
};
*/

Promise.myAllSettled = function (promises) {
  return new Promise((resolve, reject) => {
    if (!promises || promises.length === 0) {
      resolve([]);
    }
    const len = promises.length;
    let count = 0;
    const results = new Array(len);

    promises.forEach((promise, index) => {
      Promise.resolve(promise)
        .then((res) => {
          results[index] = {
            status: "fulfilled",
            value: res,
          };
        })
        .catch((err) => {
          results[index] = {
            status: "rejected",
            error: err,
          };
        })
        .finally(() => {
          count++;
          if (count === len) {
            resolve(results);
          }
        });
    });
  });
};
