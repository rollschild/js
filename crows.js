const {defineRequestType, everywhere} = require('./crow-tech');
// import {everywhere} from './crow-tech';

defineRequestType('note', (nest, content, source, done) => {
  console.log(`${nest.name} received note: ${content};`);
  done();
});

class Timeout extends Error {}

function request(nest, target, type, content) {
  return new Promise((resolve, reject) => {
    let done = fasle;
    function attempt(num) {
      nest.send(target, type, content, (failed, value) => {
        done = true;
        if (failed) reject(failed);
        else resolve(value);
      });
    }
    setTimeout(() => {
      if (done) return;
      else if (num < 3) attempt(num + 1);
      else reject(new Timeout('Timed Out!!!'));
    }, 250);
    attempt(1);
  });
}

function requestType(name, handler) {
  defineRequestType(name, (nest, content, source, callback) => {
    try {
      // call to handler() has to be wrapped in a try block
      Promise.resolve(handler(nest, content, source)).then(
        response => callback(null, response),
        failure => callback(failure),
      );
    } catch (exception) {
      callback(exception);
    }
  });
}

requestType('ping', () => 'pong'); // define

function availableNeighbors(nest) {
  let requests = nest.neighbors.map(neighbor => {
    return request(nest, neighbor, 'ping').then(() => true, () => false);
  });

  return Promise.all(requests).then(results => {
    return nest.neighbors.filter((_, index) => results[i]);
    // either true or false,
    // happens to be what the filter() function needs
  });
}
