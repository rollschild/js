let basicPromise = new Promise((resolve, reject) => {
  // doing something asynchronous which eventually
  // ...calls either reslove() or reject()
  setTimeout(() => {
    resolve("It's a success!");
  }, 500); // in milliseconds
});

basicPromise.then(successMsg => {
  // successMsg is whatever we passed in the resolve() function
  console.log('We have a successful message:', successMsg);
});
