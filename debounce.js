function debounce(callback, delay, immediate = false) {
  let debouncedId;

  return function debounced(...args) {
    clearTimeout(debouncedId);

    if (immediate && !debouncedId) {
      callback.apply(this, args);
    }

    debouncedId = setTimeout(() => {
      if (!immediate) {
        callback.apply(this, args);
      }
      debouncedId = null;
    }, delay);
  };
}

// Do not edit the line below.
exports.debounce = debounce;
