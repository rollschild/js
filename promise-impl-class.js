const STATE = {
  PENDING: "pending",
  FULFILLED: "fulfilled",
  REJECTED: "rejected",
};

class MyPromise {
  #value = null;
  #state = STATE.PENDING;
  #onFulfilledChain = [];
  #onRejectedChain = [];

  constructor(executorFunc) {
    try {
      executorFunc(
        (value) => this.#resolve(value),
        (value) => this.#reject(value)
      );
    } catch (error) {
      this.#reject(error);
    }
  }

  #resolve(value) {
    this.#value = value;
    this.#state = STATE.FULFILLED;
    this.#onFulfilledChain.forEach((callback) => callback());
  }

  #reject(value) {
    this.#value = value;
    this.#state = STATE.REJECTED;
    this.#onRejectedChain.forEach((callback) => callback());
  }

  then(onFulfilled, onRejected) {
    return new MyPromise((resolve, reject) => {
      const onFulfilledCallback = () => {
        if (!onFulfilled) {
          return resolve(this.#value);
        }

        queueMicrotask(() => {
          try {
            const value = onFulfilled(this.#value);
            resolve(value);
          } catch (error) {
            reject(error);
          }
        });
      };
      const onRejectedCallback = () => {
        if (!onRejected) {
          return reject(this.#value);
        }

        queueMicrotask(() => {
          try {
            const value = onRejected(this.#value);
            resolve(value);
          } catch (error) {
            reject(error);
          }
        });
      };

      switch (this.#state) {
        case STATE.PENDING:
          // here, `this` refers to the newly returned MyPromise
          this.#onFulfilledChain.push(onFulfilledCallback);
          this.#onRejectedChain.push(onRejectedCallback);
          break;
        case STATE.FULFILLED:
          onFulfilledCallback();
          break;
        case STATE.REJECTED:
          onRejectedCallback();
          break;
        default:
          throw new Error("Unrecognized promise state!");
      }
    });
  }

  catch(onRejected) {
    return this.then(null, onRejected);
  }

  get state() {
    return this.#state;
  }

  get value() {
    return this.#value;
  }
}

// Do not edit the line below.
exports.MyPromise = MyPromise;
