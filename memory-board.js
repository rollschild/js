import React from "react";

const TILE_COLORS = ["red", "green", "blue", "yellow"];

export default function Memory() {
  // Write your code here.
  const [selectable, setSelectable] = useState(true);
  const [selections, setSelections] = useState([]); // at most 2 elements
  const [colors, setColors] = useState(
    shuffle([...TILE_COLORS, ...TILE_COLORS])
  );
  const [classNames, setClassNames] = useState(new Array(8).fill(""));

  const won = classNames.every((className) => className !== "");
  const handleRestart = () => {
    setSelections([]);
    setColors(shuffle([...colors]));
    setClassNames(new Array(8).fill(""));
  };

  const title = won ? "You Win!" : "Memory";
  const restart = won ? <button onClick={handleRestart}>Restart</button> : null;

  const handleClick = (e, id) => {
    e.preventDefault();
    if (classNames[id] !== "" || !selectable || selections.length === 2) {
      return;
    }
    // paint
    const newClassNames = [...classNames];
    newClassNames[id] = colors[id];
    setClassNames(newClassNames);

    const newSelections = [...selections, id];
    if (newSelections.length === 2) {
      compare(newSelections);
    } else {
      setSelections(newSelections);
    }
  };

  const compare = ([id1, id2]) => {
    if (colors[id1] === colors[id2]) {
      // leave the paint
      setSelections([]);
    } else {
      setSelectable(false);
      setTimeout(() => {
        setSelections([]);
        const newClassNames = [...classNames];
        newClassNames[id1] = "";
        newClassNames[id2] = "";
        setClassNames(newClassNames);
        setSelectable(true);
      }, 1000);
    }
  };

  return (
    <>
      <h1>{title}</h1>
      <div className="board"></div>
      {colors.map((color, index) => {
        const className = classNames[index];
        return (
          <div
            className={"tile" + (className ? ` ${className}` : "")}
            key={index}
            id={index}
            onClick={(e) => handleClick(e, index)}
            disabled={!selectable}
          ></div>
        );
      })}
      {restart}
    </>
  );
}

/**
 * Returns the array shuffled into a random order.
 * Do not edit this function.
 */
function shuffle(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const randomIndex = Math.floor(Math.random() * (i + 1));

    // Swap the elements at i and randomIndex
    [array[i], array[randomIndex]] = [array[randomIndex], array[i]];
  }
  return array;
}
