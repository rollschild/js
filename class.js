class Rabbit {
  constructor(type) {
    this.type = type;
  }

  speak(line) {
    console.log(`The ${this.type} rabbit says ${line}`);
  }
}

Rabbit.teeth = 'small';

let killerRabbit = new Rabbit('killer');
console.log(1, killerRabbit.teeth);
console.log(2, killerRabbit.speak('cutieee..'));
// Rabbit.teeth = "small";
// console.log(Rabbit.teeth);
Rabbit.prototype.teeth = 'small';
console.log(3, killerRabbit.teeth);
killerRabbit.teeth = 'sharp and bloody';
console.log(4, Rabbit.prototype.teeth);
console.log(5, killerRabbit.teeth);
console.log(6, Rabbit);
