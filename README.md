# JS

Some generic JavaScript stuff.

## JS and the Browser

- `<!doctype html>`
- in HTML, an ampersand (`&`) followed by a name or character code _and_
  a semicolon (`;`) is called an _entity_ and will be replaced by the
  character it encodes
- load ES modules by giving the script tag a `type="module"` attribute
- _Document Object Model_
- the global binding `document` gives access to DOM
  - its `documentElement` property refers to the object representing the
    `<html>` tag
- in DOM, `document.documentElement` is the root
- `document.body`
- each DOM node object has a `nodeType` property, which contains a code (number)
  - elements have code 1
    - `Node.ELEMENT_NODE`
  - text nodes have code 3
    - `Node.TEXT_NODE`
  - comments have code 8
    - `Node.COMMENT_NODE`
- `childNodes`: a property that element nodes in DOM have
  - holds an array-like object
  - with a `length` property
  - but it's of type `NodeList`
  - _not_ a real array
  - doesn't have methods such as `slice` and `map`
- every node has a `parentNode` property
- `firstChild` and `lastChild`
  - `null` for nodes without children
- `previousSibling` and `nextSibling`
- `children` vs. `childNodes`
  - `children` _only_ contains element (type 1) children, not other types of
    child nodes
- `nodeValue` of a text node holds the string of text
- all element nodes have a `getElementsByTagName` method
- `alt`: specifies an alternative textual representation of the image
- to create a text node: `document.createTextNode`

### Create a text node

```js
<script>
  function replaceImages() {
    let images = document.body.getElementsByTagName("img");
    for (let i = images.length - i; i >= 0; i--) {
      let image = images[i];
      if (image.alt) {
        let text = document.createTextNode(image.alt);
        image.parentNode.replaceChild(text, image);
      }
    }
  }
</script>
```

- the loop goes over the images in **reverse** order
- it's important!
- the node list returned by `getElementsByTagName` (or a property like
  `childNodes`) is **live**
- the list is updated as the document changes!
- to get a static array, use `Array.from()`

### Create an element node

- `document.createElement`

```js
<blockquote id="quote">
  No book can ever be finished. While working on it we learn just enough to
  find it immature the moment we turn away from it
</blockquote>

<script>
  function elt(nodeType, ...children) {
    let node = document.createElement(nodeType);
    for (let child of children) {
      if (typeof child !== "string") {
        node.appendChild(child);
      } else {
        node.appendChild(document.createTextNode(child))
      }
    }
    return node;
  }

  document.getElementById("quote").appendChild(
    elt("footer", "--",
        elt("strong", "Karl Popper"),
        ", preface to the second edition of ",
        elt("em", "The Open Society and Its Enemies"),
        ", 1950")
  );
</script>
```

### Attributes

- some element attributes, such as `href` on links, can be accessed through
  a property of the same name on the element's DOM object
  - for most commonly used standard attributes
- for your custom attributes, you need `getAttribute` and `setAttribute`
- `class` vs. `className` in JavaScript

### Layout

- `<p>` and `<h1>` take up the whole width of the document
  - **block** elements
- `<a>` and `<strong>` are rendered on the same line with surrounding text
  - **inline** elements
- to get the size and position of an element via JavaScript,
  - `offsetWidth` and `offsetHeight` - space the element takes up
    - in **pixels**
    - **pixel**: basic unit of measurement in browser
    - used to be the smallest dot the screen can draw
  - `clientWidth` and `clientHeight` - size of the space **inside** the
    element, **ignoring** border width
- to get the precise position of an element
  - `getBoundingClientRect`
    - returns an object with properties:
      - `top`
      - `bottom`
      - `left`
      - `right`
    - indicating the pixel positions of the sides of the element relative to
      the top left of the **screen**
    - if relative to document, then add the current scroll position
      - `pageXOffset` and `pageYOffset`

### Styling

- a `style` attribute may contain one or more **declarations**
- `display: none;` hides the element
- in JavaScript, `style.fontFamily`

### Cascading styles

- **cascading** - multiple such rules are combined together
- **specificity**
- `p > a {...}` - all `<a>` tags that are **direct** children of `<p>` tags
- `p a {...}` - all `<a>` tags inside `<p>` tag

### Query selectors

- `querySelectorAll` method
  - both on the `document` object and on element nodes
  - takes a selector string and returns a `NodeList` with all element it
    matches
  - the object returned is **NOT** live
- `querySelector` - returns the first matching element

### Positioning and animating

- `position` by default is `static`
- `position: relative;` - move it to the normal place based on `left` and `top`
- `position: absolute;`

  - element removed from normal document flow
  - absolutely positioned by `top` and `left` relative to the top-left corner
    of the nearest enclosing element
    - whose `position` is **NOT** `static`
  - or relevant to the document if no such enclosing element

- browser does **NOT** update the display while a JS program is running
  - nor does it allow any interaction with the page
- we need `requestAnimationFrame` to let the browser know we are done for now

## Event Handling

### Events and DOM nodes

- `window` - a built-in object provided by the browser
  - represents the browser window that contains the document
- `addEventListener`
- only one event handler can be registered on the node via `onclick`
- but `addEventListener` can add any number of handlers
- `removeEventListener`

### Event objects

- event handler functions are passed an argument - the **event object**
- `event.button` === `0`, `1`, or `2`

### Propagation

- for most event types, handlers registered on nodes with children will also
  receive events that happen in the children
- but if both parent and child have a handler,
  - the more specific handler - the one one the child - goes first
  - **propagate** outward
    - from the node where it happened to that node's parent and on to the root
- after all handlers registered on a specific node had their turn, handlers
  registered on the whole window get a chance to respond to the event
- `stopPropagation`
  - at any point, an event handler can call the `stopPropagation` method **on the
    event object** to prevent handlers further **up** from receiving the event
- most event objects have a `target` property that refers to the node where
  they originated
  - `event.target.textContent`

### Default actions

- many events have a default action associated
- for _most_ types of events, the JS event handlers are called **before** the
  dedefault behavior takes place
  - call `preventDefault` to prevent the default behavior from happening

### Key events

- when a key on the keyboard is pressed, browser fires a `keydown` event
  - when key is pressed & held, `keydown` is fired again every time the key
    _repeats_
  - you might acidentally add lots of buttons when the key is held down longer
- when key release, a `keyup` is fired
- `event.key`
- modifier keys, SHIFT, CTRL, ALT, and meta (COMMAND on macOS) generate normal
  key events
  - but when combined with normal keys, take a look at
    - `event.shiftKey`
    - `event.ctrlKey`
    - `event.altKey`
    - `event.metaKey`

```js
if (event.key === " " && event.ctrlKey) {
}
```

- the DOM node, where a key event originates, depends on the element that has
  focus when the key is pressed
  - most nodes **cannot** have focus unless given a `tabindex` attribute
  - but links, buttons, and form fields can
  - when nothing in particular has focus, `document.body` is the target node of
    key events
- `input` event: when content changes in `<input>` or `<textarea>`
- read the content directly from the focused field!

### Pointer events

- mouse and touchscreen produce different kinds of events

#### Mouse clicks

- when pressing a mouse,
  - `mousedown`
  - `mouseup`
    - on DOM nodes immediately below the pointer when event occurs
  - `click` event
    - on the _most specific_ node that contained **both** the press and release of
      the button
- `dbclick`
- to get precise info about where the mouse event happened:
  - `clientX` and `clientY` - relative to the window
  - or `pageX` and `pageY` - relative to the document
- `mousemove` - every time pointer is moved
- `event.button` vs. `event.buttons`
  - `event.buttons` indicates the buttons that are currently held down
    - left buttons has code `1`, right button `2`, middle one `4`

#### Touch events

- `touchstart`
- `touchmove`
- `touchend`
- the event object instead has a `touches` property, which is an array where
  each element has:
  - `clientX`, `clientY`, `pageX`, `pageY`

#### Scroll events

- `position: fixed` - similar to `absolute`
  - also prevents node from scrolling along with the rest of the document
- `innerHeight` - height of the window
- `innerWidth` - width of the window
- `scrollHeight` - total scrollable height
- the event handler is called only **after** the scrolling takes place

```js
window.addEventListener("scroll", () => {
  let max = document.body.scrollHeight - innerHeight;
  bar.style.width = `${(pageYOffset / max) * 100}%`;
});
```

#### Focus events

- `focus`
- `blur`
- focus event does **NOT** propagate!
  - handler on parent is **NOT** notified when a child element gains/loses
    focus

#### Load events

- `load` event: fires on the window and the document body objects, when a page
  finishes loading
- often used to schedule initialization actions that require the whole document
  to have been built
- images and script tags that load an external file also have a `load` event
- does **NOT** propagate
- content of the `<script>` tag is run **immediately** when encountered
- `beforeunload`:
  - fires when a page is closed or navigated away from
  - prevent user from accidentally closing the document
  - by returning a non-null value from the handler

#### Events and the event loop

- **web worker**: a JS process that runs alongside the main script, on its own
  timeline
- workers do **NOT** share their global scope or any other data with the main
  script's environment
  - to avoid the problem of having multiple threads touching the same data
  - instead, you have to communicate with them by sending messages back and
    forth
- only JSON values can be sent as messages
- receiver receives a **copy** of the message, rather than the value itself
- communicating via the `Worker` object

#### Timers

- `clearTimeout`
- `cancelAnimationFrame` works in the same way as `clearTimeout`
  - calling it on a value returned by `requestAnimationFrame` will cancel that
    frame
- `setInterval` & `clearInterval`

#### Debouncing

- use `setTimeout`

### Drawing on canvas

- **Scalable Vector Graphics** (SVG)
  - a document markup dialect that focuses on shapes rather than text
  - can embed SVG document directly in HTML document or include it with an
    `<img>` tag
- **canvas**
  - a single DOM element that encapsulates a picture
  - provides a programming interface for drawing shapes onto the space taken up
    by the node
- svg vs. canvas
  - in svg the original description of the shapes is preserved
    - can be moved/resized at any time
  - canvas converts shapes to pixels as soon as they are drawn
    - does **NOT** remember what the pixels represent
    - the only way to move a shape on a canvas is to clear the shape and redraw

#### SVG

```html
<svg xmlns="http://www.x3.org/2000/svg"></svg>
```

- the `xmlns` attribute changes an element (and its children) to a different XML
  _namespace_

#### Canvas

- drawn onto `<canvas>`
- `<canvas>` tag is intended to allow different styles of drawing
- to get access to the actual drawing interface, we need a **context**
  - an object whose methods provide the drawing interface
- currently two mainstream drawing styles
  - `2d`
  - `webgl` - three-dimensional through the OpenGL interface

```html
<p>Before canvas.</p>
<canvas width="120" height="60"></canvas>
<p>After canvas.</p>
<script>
  let canvas = document.querySelector("canvas");
  let context = canvas.getContext("canvas");
  context.fillStyle = "red";
  context.fillRect(10, 10, 100, 50);
</script>
```

- coordinate system starts from top-left corner

#### Lines and surfaces

- in canvas, a shape can be _filled_ or _stroked_
- `fillRect` and `strokeRect`
  - they don't specify color of the fill, or thickness of the stroke
  - those are determined by the context object
  - `fillStyle`
  - `strokeStyle`
  - `lineWidth`
- if no `width` or `height` specified, then default width of 300px and height
  of 150px

#### Paths

- a path is a sequence of lines
- in 2d canvas, path is done through side effects
  - make a sequence of method calls to describe the shape

```html
<canvas></canvas>
<script>
  let cx = document.querySelector("cavas").getContext("2d");
  cx.beginPath();
  for (let y = 10, y < 100; y += 10) {
    cx.moveTo(10, y);
    cx.lineTo(90, y);
  }
  cx.stroke();
</script>
```

- when filling a path, each shape is filled separately
- a path can contain multiple shapes - each `moveTo` motion starts a new one
- but path needs to be closed before being filled
  - the start and end are in same position
  - if not closed, a line automatically added from end to start
- `closePath` - explicitly close a path by adding an actual line segment back
  to the start

#### Curves

- `quadraticCurve`
  - draws a curve to a give point
  - to determine curvature - a control point
    - attracting the line towards the control point
- `bezierCurve`
  - two control points
- `arc`
  - a line drawn with `arc` is connected to the previous path segment
  - use `moveTo` to start a new path to avoid this ^

#### Text

- `fillText` and `strokeText`
- `strokeText`: outline letters
- last two arguments `fillText` takes indicate the position of the start of the
  text's alphabetic baseline
  - to change horizontal position:
    - set `textAlign` to `end` or `center`
  - to change vertical position:
    - set `textBaseline` to `top`, `middle`, or `bottom`

#### Images

- in computer graphics,
  - **vector** vs. **bitmap**
- **vector**: specifies an image by giving a logical description of shapes
- **bitmap**: doesn't specify actual shapes but rather work with pixel data
- `drawImage`: draw pixel data onto canvas
  - if given **9** arguments, can be used to draw only a fragment of the image
  - can be used to pack multiple sprites (image elements) into a single image
    file then draw only the part you need
- to animate a picture on canvas, use `clearRect`
  - similar to `fillRect`
  - but makes the rectangle transparent

#### Transformation

- `scale`: calling it will cause anything drawn after it to be scaled
  - two parameters: horizontal and vertical
  - everything about the drawn image, including the line width, to be stretched
    out or squeezed together
- scale by a negative amount will flip the image around
  - happens on point (0, 0)
  - also flips the direction of the coordinate system
- `rotate` and `translate`
  - they **stack**
    - each transformation happens relative to the previous one
- to flip a picture around the vertical line

```js
function flipHorizontally(context, around) {
  context.translate(around, 0);
  context.scale(-1, 1);
  context.translate(-around, 0);
}
```

#### Storing and clearing transformations

- the `save` and `restore` methods in the 2D canvas context
- can also call `resetTransform` to fully reset the transformation

## HTTP and Forms

### The protocol

- `Host` header is required in a request
- `<form>`
  - when its `method` attribute is `GET` (or omitted), the info in the form is
    added to the end of the `action` URL as a **query string**
  - if `method` is `POST`, then the query string will be put in the body of the
    request
- **URL encoding**: uses a percent sign followed by two hexadecimal digits that
  encode the character code
  - `encodeURIComponent` and `decodeURIComponent` in JS
- `GET` requests should be used for requests that do **NOT** have side effects

### Fetch

- `fetch`: the interface through which browser JS makes HTTP requests
  - uses Promises
  - returns a promise that resolves to a `Response` object
  - `headers.get("Content-Type")`
  - the promise resolves successfully even if the server responded with error
    code
  - to get the actual content of a response, `res.text()`
  - the initial promise is resolved as soon as the response's headers have been
    received
    - reading the response body might take a while longer
    - thus this again returns a promise
- by default `fetch` uses `GET` and does **NOT** include a request body

```js
fetch("example/data.txt", { headers: { Range: "bytes=8-19" } })
  .then(res => res.text())
  .then(console.log);
```

### HTTP sandboxing

- browsers disallow scripts to make HTTP requests to other domains
- server indicates: `Access-Control-Allow-Origin: *`

- **remote procedure call**

### Form fields

- `<input>`
  - `type` attribute
    - `text`
    - `password`
    - `checkbox`
    - `radio`
    - `file`
- `<textarea>` - multiline
- `<select>`

### Focus

- form fields can get **keyboard focus**
- use `foucs` and `blur` to control focus
- `document.activeElement`: currently focused element
- browser provides the `autofoucs` attribute
- use `TAB` to move foucs through
  - `tabindex`
    - `-1`: skip this element when tabbing, even if it's normally focusable

### Disabled fields

- `disabled`

### Form as a whole

- the field's DOM element has a `form` property linking back to the form's DOM
  element
- the `<form>` element has a property called `elements`
  - contains an array-like collection of the fields in the form
- `name` attribute of a field
  - determines the way its value will be identified when submitted
  - also can be used as a property name when accessing the form's `elements`
    - which, acts both as an array-like object (accessible by number) and a map
      (accessible by name)
- pressing `ENTER` when form field is focused === submit
- `submit` event

### Text fields

- text fields, such as `<input>` or `<textarea>` with a type of `text` or
  `password` share a common interface
  - a `value` property that holds the current content as `string`
  - setting this to another string changes the content
- `selectionStart` and `selectionEnd`
  - cursor and selection
  - when nothing selected, they hold same number - position of cursor
  - when some text selected, they differ
- `change` event:
  - fired when field loses focus _after_ its content was changed
  - use `input` if you want to immediately respond to changes in a text field

### Checkboxes and radio buttons

- the `checked` property: a boolean
- the `<label>` tag associates a piece of document with an input field
  - clicking anywhere on the label will activate the field
  - focues the field and toggles its value
- a radio button is similar to a checkbox
  - but implicitly linked to other radio buttons with the same `name` attribute
  - so that only one of them can be active at any time

```html
<label> <input type="radio" name="color" value="orange" /> Orage </label>
```

### Select fields

- `multiple`
- each `option` tag has a value, via `value`
  - when not given, text inside the option will count as the value
- the `value` property of a `<select>`: the currently selected option
- for a `multiple` field, this doesn't mean much - only one of the selected
  options
- the `<option>` tags can be accessed via the fields `options` property
- each options has a `selected` property
- `Array.from(select.options)`

### File fields

- `<input type="file">`
- `input.files`
  - array-like object
- `file.name`
- `file.size`
- `file.type`
  - `text/plain`
  - `image/jpeg`
- reading a file
  - by creating a `FileReader` object
  - register a `load` event handler for it
  - call its `readAsText` method
  - once loading finishes, `reader.result` contains the file's content
- `FileReader` also returns an `error` **event**
  - the error object is in `reader.error`

```js
function readFileText(file) {
  return new Promise((resolve, reject) => {
    let reader = new FileReader();
    reader.addEventListener("load", () => resolve(reader.result));
    reader.addEventListener("error", () => reject(reader.error));
  });
}
```

### Storing data client-side

- `localStorage`
  - store data that survive page reloads
  - `setItem()`
  - `getItem()`
  - `removeItem()`
- `localStorage` vs. `sessionStorage`
  - content of `sessionStorage` is forgotten after each **session**
    - whenever the browser is closed

## JavaScript and Performance

### Staged compilation

- JS compilers do **NOT** simply compile a program once
  - code is compiled and recompiled while the program is running
- when a website is opened, scripts are first compiled in a cheap/superficial
way
  - doesn't result in very fast execution
  - but allows the script to start quickly
  - functions may not be compiled at all until the first time called
- when running the program, JS engine observes how often each piece of code
runs
  - when some code consumes large amount of time (hot code) - recompiled by
  a more advanced but slower compiler
  - more optimizations
  - faster code

### Graph layout

- **planar** graph: edges not crossing each other
- **force-directed graph layout**
- by replacing `for/of` loops with regular `for` loops,
  - code execution on Firefox/Edge faster
  - by simply not using iterators

### Profiling

- **micro-optimization**
- **observe**
- use the **profiler** tool provided by browsers

### Function inlining

- compiler inlines functions
- **deoptimize**

### Garbage collection

- code that avoids creating objects is faster
- **generational garbage collection**
  - splits the memory available to the JS program into two or more
  **generations**
  - new objects are created in the space reserved for the young generation

### Dynamic types

- in JS, **bindings** do not have types
- only **values** have types
- if a piece of code needs to be fast, 
  - feed it consistent types
