import React, { useState } from "react";

export default function TipCalculator() {
  const [bill, setBill] = useState("50");
  const [tip, setTip] = useState("18");
  const [people, setPeople] = useState("1");

  const handleBillChange = (e) => setBill(e.target.value);
  const handleTipChange = (e) => setTip(e.target.value);
  const handlePeopleChange = (e) => setPeople(e.target.value);
  const billNumber = parseFloat(bill);
  const tipNumber = parseFloat(tip);
  const peopleNumber = parseInt(people);
  const totalTip =
    isNaN(billNumber) || isNaN(tipNumber)
      ? NaN
      : (billNumber * tipNumber) / 100;
  const tpp =
    isNaN(totalTip) || isNaN(peopleNumber) ? NaN : totalTip / peopleNumber;

  return (
    <>
      <label htmlFor="bill">Bill</label>
      <input
        type="number"
        id="bill"
        name="bill"
        value={bill}
        onChange={handleBillChange}
      />
      <label htmlFor="tip">Tip Percentage</label>
      <input
        type="number"
        id="tip"
        name="tip"
        value={tip}
        onChange={handleTipChange}
      />
      <label htmlFor="people">Number of People</label>
      <input
        type="number"
        id="people"
        name="people"
        value={people}
        onChange={handlePeopleChange}
      />
      <p>Total Tip: {isNaN(totalTip) ? "-" : `$${totalTip.toFixed(2)}`}</p>
      <p>Tip Per Person: {isNaN(tpp) ? "-" : `$${tpp.toFixed(2)}`}</p>
    </>
  );
}
