function removeDuplicates(arr) {
  let exists = {};
  let output = [];

  arr.forEach((val, index) => {
    if (!exists[val]) {
      output.push(val);
      exists[val] = true;
    }
  });

  return output;
}
console.log([12, 2037, -1, 22, 12, -1, -2, 100]);
console.log(removeDuplicates([12, 2037, -1, 22, 12, -1, -2, 100]));
