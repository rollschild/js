let start = Date.now();

setTimeout(() => {
  console.log('Timeout ran at', Date.now() - start);
}, 20);

while (Date.now() < start + 50) {}

console.log('Wasted time until', Date.now() - start);

Promise.resolve('Done Immediately!').then(console.log);
console.log('This thing runs first!');

/* Wasted time until 50
 * This thing runs first!
 * Done Immediately!
 * Timeout ran at 53 */
