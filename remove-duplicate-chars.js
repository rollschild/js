function remove(str) {
  let count = {};
  let backup = str;
  let res = '';

  for (let i = 0; i < backup.length; ++i) {
    let c = backup[i].toLowerCase();
    if (count[c]) count[c]++;
    else count[c] = 1;
  }

  for (let key of Object.keys(count)) {
    if (count[key] === 1) res += key;
  }

  return res;
}

console.log(remove('Learn more javascript dude'));
