function getDate(string) {
  let [_, month, day, year] = /(\d{1,2})\/(\d{1,2})\/(\d{4})/.exec(string);
  return new Date(year, month - 1, day);
}

console.log(getDate('01/27/2019'));
