new Promise((resolve, reject) => {
  resolve('Resolved successfully!');
  reject(new Error('Failed and rejected!'));
})
  .then(val => console.log('Results: ' + val))
  .catch(reason => {
    console.log('Caught failure: ' + reason);
    return 'Returned from catch()';
  })
  .then(val => console.log('After catch(): ' + val));
