console.log(
  'Shi, Guangchu\nZhang, Yiwen\nWang, Kun'.replace(/(\w+), (\w+)/g, '$2 $1 $&'),
);

let stock = '1 orange, 12 eggs, 2 beefs, and 1 bottle of milk';

function minusOne(match, amount, unit) {
  amount = Number(amount) - 1;

  if (amount === 0) {
    amount = 'no';
  } else if (amount === 1) {
    unit = unit.slice(0, unit.length - 1);
  }

  return amount + ' ' + unit;
}

// console.log(stock.replace(/\b(\d+) (\w+)\b/g, minusOne));
console.log(stock.replace(/(\d+) (\w+)/g, minusOne));
