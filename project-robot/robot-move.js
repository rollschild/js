const roadGraph = require('./meadowfield');

class RobotState {
  constructor(currPlace, parcels) {
    this.currPlace = currPlace;
    this.parcels = parcels;
  }

  moveTo(destination) {
    if (!roadGraph[this.currPlace].includes(destination)) {
      // no route
      return this;
    } else {
      let bags = this.parcels
        .map(bag => {
          if (bag.currAddr !== this.currPlace) {
            return bag;
          } else {
            return {to: bag.to, currAddr: destination};
          }
        })
        .filter(bag => bag.to !== bag.currAddr);

      return new RobotState(destination, bags);
    }
  }
}

/*
let first = new RobotState(
  "Post Office",
  [{currAddr: "Post Office", to: "Jovi's House"}]
);
let next = first.moveTo("Jovi's House");
console.log(next.currPlace);
console.log(next.parcels);
console.log(first.currPlace);
console.log(first.parcels);
*/

function randomPick(arr) {
  let choice = Math.floor(Math.random() * arr.length);
  return arr[choice];
}

function randomRobot(state) {
  return {direction: randomPick(roadGraph[state.currPlace])};
}

RobotState.random = function(parcelCount = 5) {
  let parcels = [];

  for (let i = 0; i < parcelCount; ++i) {
    let to = randomPick(Object.keys(roadGraph));
    let currAddr;
    do {
      currAddr = randomPick(Object.keys(roadGraph));
    } while (currAddr == to);

    parcels.push({to, currAddr});
  }

  return new RobotState('Post Office', parcels);
};

function runRobot(state, robot, memory) {
  for (let turn = 0; ; ++turn) {
    if (state.parcels.length === 0) {
      console.log(`All delivered in ${turn} turns..`);
      break;
    }

    let action = robot(state, memory);
    state = state.moveTo(action.direction);
    memory = action.memory;
    console.log(`Moved to ${action.direction}...`);
  }
}

// runRobot(RobotState.random(), randomRobot);
module.exports = {runRobot, RobotState};
