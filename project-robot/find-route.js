const roadGraph = require('./meadowfield');
const {runRobot, RobotState} = require('./robot-move');

function findRoute(graph, from, to) {
  let works = [{at: from, route: []}];

  for (let i = 0; i < works.length; ++i) {
    let {at, route} = works[i];
    for (let place of graph[at]) {
      if (place === to) {
        return route.concat(place);
      } else {
        if (!works.some(w => w.at === place)) {
          works.push({at: place, route: route.concat(place)});
        }
      }
    }
  }
}

function goalOrientedRobot({currPlace, parcels}, route) {
  if (route.length === 0) {
    let parcel = parcels[0];
    if (parcel.currAddr !== currPlace) {
      // Parcel NOT picked up yet
      route = findRoute(roadGraph, currPlace, parcel.currAddr);
    } else {
      // Parcel picked up but NOT delivered yet
      route = findRoute(roadGraph, currPlace, parcel.to);
    }
  }

  return {direction: route[0], memory: route.slice(1)};
}

console.log(RobotState.random());
runRobot(RobotState.random(), goalOrientedRobot, []);
