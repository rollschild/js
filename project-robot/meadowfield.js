const roads = [
  "Jovi's House - Eva's House",
  "Jovi's House - Cabin",
  "Jovi's House - Post Office",
  "Eva's House - Town Hall",
  "Dracula's House - Bear's House",
  "Dracula's House - Town Hall",
  "Bear's House - Bunny's House",
  "Bunny's House - Farm",
  "Bunny's House - Shop",
  'Marketplace - Farm',
  'Marketplace - Post Office',
  'Marketplace - Shop',
  'Marketplace - Town Hall',
  'Shop - Town Hall',
];

function buildGraph(edges) {
  let graph = Object.create(null);

  function addEdge(from, to) {
    if (!graph[from]) {
      graph[from] = [to];
    } else {
      graph[from].push(to);
    }
  }

  for (let [from, to] of edges.map(road => road.split(' - '))) {
    addEdge(from, to);
    addEdge(to, from);
  }

  return graph;
}

const roadGraph = buildGraph(roads);
// console.log(roadGraph);
module.exports = roadGraph;
