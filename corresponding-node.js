function correspondingNode(tree1, tree2, node1) {
  const path = getPathFromNode(tree1, node1);
  return getNodeFromPath(tree2, path);
}

function getPathFromNode(root, node) {
  const path = [];
  while (node !== root) {
    const parent = node.parentNode;
    const children = Array.from(parent.children);
    const index = children.indexOf(node);
    path.push(index);
    node = parent;
  }
  return path;
}

// function getNodeFromPath(root, path) {
//   let child = root;
//   while (path.length > 0) {
//     const children = Array.from(child.children);
//     child = children[path.pop()];
//   }

//   return child;
// }
function getNodeFromPath(root, path) {
  return path.reduceRight(
    (foundNode, currIndex) => Array.from(foundNode.children)[currIndex],
    root
  );
}

function correspondingNodeV2(tree1, tree2, node1) {
  const q1 = [tree1];
  const q2 = [tree2];

  while (q1.length > 0) {
    const curr1 = q1.shift();
    const curr2 = q2.shift();
    if (curr1 === node1) {
      return curr2;
    }

    q1.push(...curr1.children);
    q2.push(...curr2.children);
  }
}
