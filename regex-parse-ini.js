function parseINI(string) {
  let result = {};
  let section = result;

  string.split(/\r?\n/).forEach(line => {
    let match;
    if ((match = line.match(/^(\w+)=(.*)$/))) {
      section[match[1]] = match[2];
    } else if ((match = line.match(/^\[(.*)\]$/))) {
      result[match[1]] = {};
      section = result[match[1]];
    } else {
      if (!/^\s*(;.*)?$/.test(line)) {
        throw new Error("Line '" + line + "' is not valid!");
      }
    }
  });

  return result;
}

console.log(
  parseINI(`
name=Jovi

[address]
street1=5021 Seminary Rd
street2=Apt. 1527
city=Alexandria

[occupation]
jobtitle=Assoc. Senior Software Engineer
company=Cerner
`),
);
