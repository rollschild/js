function stripComments(code) {
  // return code.replace(/\/\/.*|\/\*[^]*\*\//g, '');
  return code.replace(/\/\/.*|\/\*[^]*?\*\//g, '');
}

console.log(stripComments('x = 10; // comments!'));
console.log(stripComments('12 /* comment */ + /* another comment */ a'));
