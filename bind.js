function Point(x, y) {
  this.x = x;
  this.y = y;
}
Point.prototype.toString = function () {
  return `${this.x},${this.y}`;
};

const emptyObj = {};
const YAxisPoint = Point.bind(emptyObj, 0);
YAxisPoint(13);

// console.log("emptyObj:", emptyObj); // {x: 0, y: 13}

const obj = {
  name: "obj",
  get: function () {
    console.log(this.name);
  },
  getFromNested: function () {
    return function () {
      console.log("from nested func:", this.name); // logs undefined
    };
  },
};

// obj.get();
// obj.getFromNested()();

class Bind {
  constructor() {
    this.name = "bind";
  }

  log() {
    console.log("this.name:", this.name);
  }

  tryLog() {
    const logfn = this.log;
    const arrowLog = () => this.log();
    // logfn(); // error out
    arrowLog(); // this.name: bind
  }
}

const ins = new Bind();
ins.tryLog();
