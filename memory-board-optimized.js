import React, { useState, useEffect } from "react";

const TILE_COLORS = ["red", "green", "blue", "yellow"];

export default function Memory() {
  // Write your code here.
  const [selectable, setSelectable] = useState(true);
  const [selections, setSelections] = useState([]); // at most 2 elements
  const [colors, setColors] = useState(
    shuffle([...TILE_COLORS, ...TILE_COLORS])
  );
  const [matches, setMatches] = useState([]);

  const won = matches.length === 8;
  const handleRestart = () => {
    setSelections([]);
    setColors(shuffle([...colors]));
    setMatches([]);
  };

  const title = won ? "You Win!" : "Memory";
  const restart = won ? <button onClick={handleRestart}>Restart</button> : null;

  useEffect(() => {
    if (selections.length < 2) {
      return;
    }
    const compare = ([id1, id2]) => {
      if (colors[id1] === colors[id2]) {
        // leave the paint
        setSelections([]);
        setMatches((matches) => [...matches, id1, id2]);
      } else {
        setSelectable(false);
        const timerId = setTimeout(() => {
          setSelections([]);
          setSelectable(true);
        }, 1000);
        return () => clearTimeout(timerId);
      }
    };
    compare(selections);
  }, [selections]);

  const handleClick = (e, id) => {
    e.preventDefault();
    if (!selectable || selections.includes(id) || selections.length === 2) {
      return;
    }
    setSelections((selections) => [...selections, id]);
  };

  return (
    <>
      <h1>{title}</h1>
      <div className="board"></div>
      {colors.map((color, index) => {
        const className =
          selections.includes(index) || matches.includes(index)
            ? ` ${colors[index]}`
            : "";
        return (
          <div
            className={"tile" + className}
            key={index}
            id={index}
            onClick={(e) => handleClick(e, index)}
            disabled={!selectable}
          ></div>
        );
      })}
      {restart}
    </>
  );
}

/**
 * Returns the array shuffled into a random order.
 * Do not edit this function.
 */
function shuffle(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const randomIndex = Math.floor(Math.random() * (i + 1));

    // Swap the elements at i and randomIndex
    [array[i], array[randomIndex]] = [array[randomIndex], array[i]];
  }
  return array;
}
