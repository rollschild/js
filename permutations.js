function permutate(str) {
  let res = [];
  let arr = str.split('');
  recursion(arr, res, []);
  return res;
}

function recursion(arr, res, sub) {
  if (sub.length === arr.length) {
    let tmp = sub.join('');
    if (res.indexOf(tmp) === -1) res.push(tmp);
    return;
  }

  for (let i = 0; i < arr.length; ++i) {
    let c = arr[i];
    let tmpArr = arr.slice(0);
    tmpArr.splice(i, 1);
    sub.push(c);
    recursion(tmpArr, res, sub);
    sub.pop();
  }
}

console.log(permutate('aabfffff'));
