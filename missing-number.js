// arrray of 1 ... n but UNsorted
function missingNumber(arr) {
  let len = arr.length + 1;
  let expected = ((1 + len) * len) / 2;

  let sum = arr.reduce((accumulator, value) => (accumulator += value));

  return expected - sum;
}

console.log(missingNumber([5, 2, 6, 1, 3]));
