function curry(callback) {
  return (function nextCurried(prevArgs) {
    return function curried(...nextArgs) {
      if (!nextArgs || nextArgs.length === 0) {
        return callback(...prevArgs);
      }
      const args = [...prevArgs, ...nextArgs];
      return nextCurried(args);
    };
  })([]);
}

const sum = (...numbers) => numbers.reduce((sum, curr) => sum + curr, 0);
const curried = curry(sum);
console.log(curried(1)(2)(3)());
