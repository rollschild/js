function merge(arrOne, arrTwo) {
  let res = [];
  let i = 0;
  let j = 0;

  if (arrOne.length === 0) return arrTwo;
  if (arrTwo.length === 0) return arrOne;

  while (arrOne[i] || arrTwo[j]) {
    if (!arrTwo[j] || arrOne[i] < arrTwo[j]) {
      res.push(arrOne[i++]);
    } else {
      res.push(arrTwo[j++]);
    }
  }

  return res;
}

console.log(merge([2, 5, 6, 9], [1, 2, 3, 29]));
